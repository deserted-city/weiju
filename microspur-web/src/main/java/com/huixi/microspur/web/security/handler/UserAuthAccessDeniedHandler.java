package com.huixi.microspur.web.security.handler;

import com.huixi.microspur.commons.errorcode.ErrorCodeEnum;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.util.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description 暂无权限处理类
 * @Author Sans
 * @CreateTime 2019/10/3 8:39
 */
@Slf4j
@Component
public class UserAuthAccessDeniedHandler implements AccessDeniedHandler {

    /**
     * 暂无权限返回结果
     * @Author Sans
     * @CreateTime 2019/10/3 8:41
     */
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException exception){

        ResultUtil.responseJson(response, Wrapper.error(ErrorCodeEnum.FORBIDDEN));

    }


}
