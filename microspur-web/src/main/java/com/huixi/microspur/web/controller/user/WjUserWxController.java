package com.huixi.microspur.web.controller.user;


import com.huixi.microspur.commons.base.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 专门用来存储微信后台发送给我们的数据 前端控制器
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@RestController
@RequestMapping("/wjUserWx")
public class WjUserWxController extends BaseController {

}

