package com.huixi.microspur.web.security.handler;

import cn.hutool.core.exceptions.ExceptionUtil;
import com.huixi.microspur.commons.errorcode.ErrorCodeEnum;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.util.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 用户未登录处理类
 * @Author Sans
 * @CreateTime 2019/10/3 8:55
 */
@Slf4j
@Component
public class UserAuthenticationEntryPointHandler implements AuthenticationEntryPoint {


    /**
     * 用户未登录返回结果
     * @Author Sans
     * @CreateTime 2019/10/3 9:01
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception){
        if (ExceptionUtil.isFromOrSuppressedThrowable(exception, InsufficientAuthenticationException.class)) {
            log.info("【登录失败】"+exception.getMessage());
            ResultUtil.responseJson(response, Wrapper.error(ErrorCodeEnum.USER_ERROR_A0301));
        }
        ResultUtil.responseJson(response, Wrapper.error(ErrorCodeEnum.UNAUTHORIZED));

    }



}
