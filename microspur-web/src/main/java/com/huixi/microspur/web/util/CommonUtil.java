package com.huixi.microspur.web.util;

import cn.hutool.core.util.IdUtil;
import com.huixi.microspur.web.pojo.entity.user.WjUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *  通用的帮助类
 * @Author 叶秋
 * @Date 2020/6/15 23:12
 * @param
 * @return
 **/
public class CommonUtil {


    /**
     *  随机创建用户信息
     * @Author 叶秋
     * @Date 2020/6/15 23:26
     * @return com.huixi.microspur.web.pojo.entity.user.WjUser
     **/
    public static WjUser randomCreateUserInfo(){


        return new WjUser().setUserId(IdUtil.simpleUUID())
                .setNickName(IdUtil.simpleUUID())
                .setRealName("你我都是微距")
                .setHeadPortrait("https://weiju-wechat.oss-cn-shenzhen.aliyuncs.com/head_portrait/luoyonghao.jpg")
                .setSex("我就不告诉你")
                .setCompany("汇溪和他们的小伙伴们")
                .setSignature("快乐源于分享")
                .setIntroduce("谢谢大家的支持😎😍")
                .setAddress("不用轻易泄露自己的地址哦，提防不安好心的人");

    }


    /**
     *  获取现在正在 调用这个接口的用户id
     * @Author 叶秋
     * @Date 2020/6/18 23:21
     * @return java.lang.String
     **/
    public static String getNowUserId(){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userId = (String) authentication.getCredentials();

        return userId;

    }


}
