package com.huixi.microspur.sysadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.sysadmin.pojo.entity.user.WjUserTrack;

/**
 * <p>
 * 用户足迹表 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjUserTrackService extends IService<WjUserTrack> {

}
