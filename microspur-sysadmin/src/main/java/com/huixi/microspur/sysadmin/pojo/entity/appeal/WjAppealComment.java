package com.huixi.microspur.sysadmin.pojo.entity.appeal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huixi.microspur.commons.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 诉求-评论
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_appeal_comment")
@ApiModel(value="WjAppealComment对象", description="诉求-评论")
public class WjAppealComment extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "诉求评论的id")
    @TableId(value = "appeal_comment_id", type = IdType.ASSIGN_UUID)
    private String appealCommentId;

    @ApiModelProperty(value = "是否是评价的评论（这里要写被评论的id，写了就是，没写就是普通的评论）")
    @TableField("about_comment")
    private String aboutComment;

    @ApiModelProperty(value = "诉求id")
    @TableField("appeal_id")
    private String appealId;

    @ApiModelProperty(value = "评论人的id")
    @TableField("user_id")
    private String userId;

    @ApiModelProperty(value = "评论的内容")
    @TableField("content")
    private String content;

    @ApiModelProperty(value = "评论所加的贴图（只能是一张）")
    @TableField("url")
    private String url;

    @ApiModelProperty(value = "评论贴图的文件类型(jpg,png,gif)")
    @TableField("file_type")
    private String fileType;

    @ApiModelProperty(value = "评论贴图大小(KB)")
    @TableField("file_size")
    private String fileSize;

    @ApiModelProperty(value = "评论的楼层")
    @TableField("floor")
    private Integer floor;

    @ApiModelProperty(value = "评论的点赞数||赞同数")
    @TableField("endorse_count")
    private Integer endorseCount;

    @ApiModelProperty(value = "对评论评论的次数")
    @TableField("comment_count")
    private Integer commentCount;

    @ApiModelProperty(value = "创建人")
    @TableField("create_by")
    private String createBy;

    @ApiModelProperty(value = "修改人")
    @TableField("update_by")
    private String updateBy;


}
