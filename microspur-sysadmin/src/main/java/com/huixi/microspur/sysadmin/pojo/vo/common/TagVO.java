package com.huixi.microspur.sysadmin.pojo.vo.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 *  这是统一给前端 返回标签值的VO  swagger需要这个，不然直接返回map 会报 no content
 * @Author 叶秋
 * @Date 2020/7/14 17:22
 **/
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Data
@ApiModel(value = "这是统一给前端 返回标签值的VO  swagger需要这个，不然直接返回map 会报 no content")
public class TagVO {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "名子")
    private String name;

}
