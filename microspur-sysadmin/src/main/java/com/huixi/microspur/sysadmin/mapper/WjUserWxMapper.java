package com.huixi.microspur.sysadmin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.sysadmin.pojo.entity.user.WjUserWx;

/**
 * <p>
 * 专门用来存储微信后台发送给我们的数据 Mapper 接口
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjUserWxMapper extends BaseMapper<WjUserWx> {

}
