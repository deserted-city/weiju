package com.huixi.microspur.sysadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.sysadmin.pojo.entity.chat.WjChat;

import java.util.List;

/**
 * <p>
 * 聊天室（聊天列表） 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjChatService extends IService<WjChat> {

    /**
     *  根据用户id 查询用户所有的聊天列表
     * @Author 叶秋
     * @Date 2020/4/23 23:19
     * @param userId
     * @return java.util.List<com.huixi.microspur.web.pojo.entity.chat.WjChat>
     **/
    List<WjChat> queryAllChat(String userId);



    /**
     *  创建诉求聊天室
     * @Author 叶秋
     * @Date 2020/7/9 22:49
     * @param appealId 诉求id
     * @param userId 调用这个方法的用户
     * @return java.lang.Boolean
     **/
    Boolean createAppealChat(String appealId, String userId);

}
